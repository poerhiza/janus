import net
import os
import strutils

import std/[tables] # provides initTable


var client: Socket

try:
    client = newSocket()
except OSError:
    echo "failed to aquire a socket"
    system.quit(0)

proc ctrlc() {.noconv.} =
    echo "Ctrl+C fired!"

    try:
        client.close()
    except:
        discard

    system.quit(0)

setControlCHook(ctrlc)

var ip = "127.0.0.1"
var port = 5556

var args = commandLineParams()

if args.len() == 2:
    ip = args[0]
    port = parseInt(args[1])
 
echo "Attempting to connect to ", ip, " on port ", port, "..."


type fptr_in_string_out_uint32 = (proc(x: string, client: Socket))

proc toByteSeq*(str: string): seq[byte] {.inline.} =
  ## Converts a string to the corresponding byte sequence.
  @(str.toOpenArrayByte(0, str.high))

proc getData(path: string, client: Socket) =
    echo "getting: ", path
    var cc = cast[array[4, byte]](1)

    if 4 != send(client, addr cc, 4):
        echo "failed to send getData cc"

    var ps = cast[array[4, byte]](len(path))

    if 4 != send(client, addr ps, 4):
        echo "failed to send getData ps"

    client.send(path)

proc badCommand(client: Socket) =
    var cc = cast[array[4, byte]](-1)

    if 4 != send(client, addr cc, 4):
        echo "failed to send badCommand cc"

    if 4 != send(client, addr cc, 4):
        echo "failed to send badCommand ps"

proc die(expected_reply: string, client: Socket) =
    echo "sending expected_reply to confirm death: ", expected_reply
    client.close()
    ctrlc()

var maxTimeoutCount = 30
var timeoutCount = 0

var headerCommands = initTable[uint32, fptr_in_string_out_uint32]()

headerCommands[1] = getData
headerCommands[4] = die

while true:

    try:
        echo "doing connection"

        client.connect(ip, Port(port), 10000)

        while true:
            var request: string = ""

            while true:
                try:
                    var buffer: string

                    if 0 != recv(client, buffer, 1, 1000):
                        request = join([request, buffer])
                    else:
                        break

                except TimeoutError:
                    # TODO: implement heartbeat command, and client.close()+break+break here
                    echo "done reading, we timed out"
                    timeoutCount.inc(1)
                    break

                except:
                    echo "Connection lost..."
                    client.close()
            
            if timeoutCount >= maxTimeoutCount:
                timeoutCount = 0
                client.close()
                echo "Time out max reached, going to sleep for a 30sec"
                sleep(30000)
                break

            if request != "":
                timeoutCount = 0
                echo "cc: |", request[0..3], "|"
                var header: seq[uint32] = cast[seq[uint32]](request)
                var commandCode: uint32 = @header[0..0][0]

                if headerCommands.hasKey(commandCode):
                    headerCommands[commandCode](request[8..^1], client)
                else:
                    badCommand(client)

                echo "got cc: ", commandCode, " ps: ", header[1..1], " payload: ", request[8..^1]

    except TimeoutError:
        echo "timeout connecting"
        sleep(10000)
        discard
    except:
        echo "something went wrong, retrying in 10 seconds..."
        sleep(10000)
        discard
    finally:
        try:
            client.close() 
        except:
            discard

    client = newSocket()
