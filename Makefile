BUILD=$(shell git rev-parse HEAD)
BASEDIR=./dist
DIR="${BASEDIR}/${BUILD}"

# Make Directory to store executables
$(shell mkdir -p ${DIR})

all: build

build:
	@nim compile --app=console --passc=-flto --passl="-Wl,--strip-debug -s -w -flto" --opt=size --debuginfo:off --cpu:amd64 --os:linux --out=${DIR}/shell -d=release -d=strip shell.nim 

clean:
	rm -rf ${BASEDIR}

.PHONY: all build clean